#[cfg(test)]
use rstest_reuse;

use std::collections::HashMap;

#[derive(Debug, Default, Clone)]
struct Fruit {
    pub id: usize,
    pub name: String,
}

trait FruitStore {
    fn count(&self) -> usize;

    fn add(&mut self, fruit: Fruit) -> Result<(), AddError>;

    fn find(&self, id: usize) -> Option<Fruit>;

    fn remove(&mut self, id: usize) -> Result<(), DeleteError>;
}

#[derive(Debug)]
enum AddError {
    DuplicateId,
}

#[derive(Debug)]
enum DeleteError {
    IdNotFound,
}

struct VecFruitStore {
    storage: Vec<Fruit>,
}

impl VecFruitStore {
    fn new() -> Self {
        VecFruitStore {
            storage: Vec::new(),
        }
    }
}

impl FruitStore for VecFruitStore {
    fn count(&self) -> usize {
        0
    }

    fn add(&mut self, fruit: Fruit) -> Result<(), AddError> {
        todo!()
    }

    fn find(&self, id: usize) -> Option<Fruit> {
        None
    }

    fn remove(&mut self, id: usize) -> Result<(), DeleteError> {
        Err(DeleteError::IdNotFound)
    }
}

struct HashMapFruitStore {
    storage: HashMap<usize, Fruit>,
}

impl HashMapFruitStore {
    fn new() -> Self {
        HashMapFruitStore {
            storage: HashMap::new(),
        }
    }
}

impl FruitStore for HashMapFruitStore {
    fn count(&self) -> usize {
        self.storage.len()
    }

    fn add(&mut self, fruit: Fruit) -> Result<(), AddError> {
        todo!();
    }

    fn find(&self, id: usize) -> Option<Fruit> {
        self.storage.get(&id).cloned()
    }

    fn remove(&mut self, id: usize) -> Result<(), DeleteError> {
        self.storage
            .remove(&id)
            .map_or(Err(DeleteError::IdNotFound), |_| Ok(()))
    }
}

#[cfg(test)]
mod tests {
    use crate::{DeleteError, FruitStore, HashMapFruitStore, VecFruitStore};
    use rstest::*;
    use rstest_reuse::*;

    #[template]
    #[rstest]
    #[case::vec_fruit_store(VecFruitStore::new())]
    #[case::hashmap_fruit_store(HashMapFruitStore::new())]
    fn fruit_store(#[case] store: impl FruitStore) {}

    #[apply(fruit_store)]
    fn empty_store_counts_zero(store: impl FruitStore) {
        assert_eq!(0, store.count());
    }

    #[apply(fruit_store)]
    fn empty_store_finds_none(store: impl FruitStore) {
        let fruit = store.find(0);

        assert!(fruit.is_none());
    }

    #[apply(fruit_store)]
    fn empty_store_remove_gives_id_not_found_error(mut store: impl FruitStore) {
        let result = store.remove(0);

        assert!(matches!(result, Err(DeleteError::IdNotFound)));
    }
}
