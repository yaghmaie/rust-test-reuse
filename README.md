# Rust Test Reuse

This repository demonstrates reusing fixture-based tests with [**rstest**](https://crates.io/crates/rstest) and [**rstest_reuse**](https://crates.io/crates/rstest_reuse).

Please read the post at [DEV Community](https://dev.to/yaghmaie/reuse-tests-with-multiple-implementations-of-a-trait-in-rust-by-rstest-5bm1).
